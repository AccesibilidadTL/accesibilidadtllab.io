---
title: "Accesibilidad con Tecnologías Libres"
description: |
  ATL es un podcast mensual producido y presentado por Jorge Lama. Con diferentes secciones donde cada "corresponsal" aporta su visión y conocimiento en temas de accesibilidad y tecnologías libres. :
  · Rompiendo barreras, con Eva Marco, en la que habla de su experiencia como desarrolladora.
  · Odisea en el ciberespacio con David Marzal, comentando aplicaciones y noticias relacionadas con el Software Libre.
  · Accesibilidad web, con Pablo Arias.
  · Otros mundos, otras realidades, con Thais Pousada
  · Laboratorio testing con Víctor y Markolino, donde nos cuentan sus impresiones sobre diferentes sistemas y aplicaciones.
  · Diseño para todos, con Jonathan Chacón
  · Y las últimas incorporaciones son "Clases con Pascal", por David Pacios y "Érase una vez", con Enrique Varela.
podcast_image: "logo-ATL-opt.png" # Fichero con el logo

author:
    name: "Jorge Lama y David Marzal"            # <itunes:author|itunes:name>, <item>-<itunes:author>
    email: "AccesibilidadTL@ProtonMail.com" # <managingEditor|webMaster|itunes:email> <item>-<author>
    fediverse: "@DavidMarzalC@masto.es" # web metadata

#URLprefix : https://media.blubrry.com/3752802/op3.dev/e,pg=68b52970-05d8-5cb1-8bd3-d15fa3961e09/
URLprefix : "https://op3.dev/e,pg=68b52970-05d8-5cb1-8bd3-d15fa3961e09/"
URLhost : "archive.org/download/accesibilidad-con-tecnologias-libres/"

# https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md
p20:
# Datos que deben estar aqui definidos si se quieren usar
    guid: "68b52970-05d8-5cb1-8bd3-d15fa3961e09"                    # channel
    protocol: "activitypub"       # https://github.com/Podcastindex-org/podcast-namespace/blob/main/socialprotocols.txt
    uri: "https://masto.es/@DavidMarzalC/114070206628547391" #channel 
# Datos especificos que pueden sustituir a los globales de hugo.toml
    locked: "no"                  # ¿Feed bloqueado para otras plataformas? yes|no
    funding: "Comenta"         # Texto para animar a colaborar
    fundingURL: "https://accesibilidadtl.gitlab.io/pages/contacto/"         # Enlace
    location : 
    - text : "España"          # channel
      GEO : "40.416944,-3.703333;u=1000000"
      OSM : "R1311341"     # España
      country: es          # Country
      rel : creator        # subject | creator
    medium : "podcast"            # podcast (default) https://podcastindex.org/namespace/1.0#medium
    transcript :
        rel : captions
        language : es
    frecuencia :
        rrule : FREQ=MONTHLY
        texto : Mensual
    participantes :
    - person : Jorge Lama
      role : Producer
      group : Creative Direction
      img : https://accesibilidadtl.gitlab.io/images/JorgeLama.jpg
      href : https://mastodon.social/@raivenra
    - person : David Marzal
      role : Reporter
      img : "https://accesibilidadtl.gitlab.io/images/DavidMarzal.png"
      href : https://masto.es/@DavidMarzalC
    - person : Jonathan Chacón
      role : Reporter
      img : "https://accesibilidadtl.gitlab.io/images/JonathnChacon.png"
      href : https://tyflosaccessiblesoftware.com/
    - person : Pablo Arias
      role : Reporter
      img : https://accesibilidadtl.gitlab.io/images/PabloArias-rd.png
      href : https://www.pabloarias.eu/
    podroll : [99aa4f6e-6667-5b2a-9097-1251f36652ac, a9a56b87-575a-5f6f-9636-cdf7b73e6230, 610e9ea8-edf0-407f-9e6c-72375a0e17db, 5a64b2bf-b6db-55f9-a342-a516c0b986e2, b78f3edc-1357-5e5c-a2f4-617d623b1197, 84c82e0c-c9d1-5a26-a236-554a968575e6]
#               Mancomún Podcast,                     KDE Express,                          Los últimos de Feedlipinas,           AliBlueBox,                            Lo que tu cuentas,                   Gafotas, Cegatos y sus Aparatos
    podping : 6706483
    websub : "https://hub.livewire.io/"
    publisher :
      feedGuid : 003af0a0-6a45-55bf-b765-68e3d349551a
      feedUrl : https://marzal.gitlab.io/mundolibre/Marzal.rss
    chat:
        server : kde.org
        protocol : matrix
        accountId : "@dmarzal:matrix.org"
        space : "#kde-es:kde.org"
skipHours: [0, 1, 2, 3, 4, 5, 20, 21, 22, 23]
skipDays: [Monday, Friday, Saturday, Sunday] # [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]
itunes:
    explicit:                   # por defecto false
    #newfeedurl: "https://kdeexpress.gitlab.io/feed" # https://podcasters.apple.com/support/837-change-the-rss-feed-url
    category: 
    - Technology :
    - News : Tech News
    - Society & Culture :
# A continuación poner la descripción del podcast con html
---
En informática, la accesibilidad incluye diferentes ayudas como pueden ser las tipografías de alto contraste o gran tamaño, magnificadores de pantalla, lectores y revisores de pantalla, programas de reconocimiento de voz, teclados adaptados y otros dispositivos apuntadores o de entrada de información.

Además, las inteligencias artificiales están empezando a ser un gran aliado para mejorar la accesibilidad en muchos aspectos. Existen podcasts y canales de vídeo que hablan de la accesibilidad centrándose en entornos Windows o de Apple porque son los más conocidos por el público generalista. Pero en este podcast queremos dar a conocer otros aspectos de la accesibilidad y su relación con otras tecnologías menos conocidas.

Tecnologías que consideramos libres y que nos parecen mejores para la sociedad, en muchos casos...
