1
00:00:00,000 --> 00:00:10,840
En informática, la accesibilidad incluye diferentes ayudas como pueden ser las tipografías

2
00:00:10,840 --> 00:00:16,600
de alto contraste o gran tamaño, magnificadores de pantalla, lectores y revisores de pantalla,

3
00:00:16,600 --> 00:00:21,280
programas de reconocimiento de voz, teclados adaptados y otros dispositivos apuntadores

4
00:00:21,280 --> 00:00:22,440
o de entrada de información.

5
00:00:22,440 --> 00:00:30,560
Además, las inteligencias artificiales están empezando a ser un gran aliado para mejorar

6
00:00:30,560 --> 00:00:34,920
la accesibilidad en muchos aspectos. Existen podcasts y canales de vídeo que hablan de

7
00:00:34,920 --> 00:00:39,600
la accesibilidad centrándose en entornos Windows o de Apple porque son los más conocidos

8
00:00:39,600 --> 00:00:44,200
por el público generalista. Pero en este podcast queremos dar a conocer otros aspectos

9
00:00:44,200 --> 00:00:47,640
de la accesibilidad y su relación con otras tecnologías menos conocidas.

10
00:00:52,560 --> 00:00:57,480
Tecnologías que consideramos libres y que nos parecen mejores para la sociedad en muchos

11
00:00:57,480 --> 00:01:02,440
casos. Pero vamos primero a conocer a los colaboradores de este podcast, sin los cuales

12
00:01:02,440 --> 00:01:04,240
no tendría esto mucho sentido.

13
00:01:04,240 --> 00:01:16,640
Contaremos con diferentes secciones donde abordaremos diferentes temas relacionados con la accesibilidad.

14
00:01:16,640 --> 00:01:21,080
Vamos a empezar por ejemplo con la sección de Banco de Pruebas. En esta sección nuestros

15
00:01:21,080 --> 00:01:24,680
colaboradores probarán diferentes soluciones que iremos proponiéndoles.

16
00:01:24,680 --> 00:01:27,680
No es así, Víctor. Te puedes presentar para la audiencia.

17
00:01:27,680 --> 00:01:38,080
Hola, muy buenas. Víctor, uso Linux desde 2016 con Ubuntu y bueno, el conocido Gnome

18
00:01:38,080 --> 00:01:43,080
2 que fue un referente. Ese es el referente que tengo accesibilidad. Podemos hablar mucho,

19
00:01:43,080 --> 00:01:49,600
lo haremos seguramente. Y me ofrezco a colaborar para mostrar qué hay de accesibilidad para

20
00:01:49,600 --> 00:01:54,000
personas con problemas visuales o difícil culta de visual y que se puede mejorar.

21
00:01:54,000 --> 00:01:58,600
Vale, queda grabado, Víctor. Tenemos tu palabra que te vas a someter a las pruebas y a los

22
00:01:58,600 --> 00:02:04,000
retos que te propongamos. Pues muchas gracias por colaborar con nosotros y puedes adelantar

23
00:02:04,000 --> 00:02:05,880
de qué vamos a tocar en el siguiente programa.

24
00:02:05,880 --> 00:02:15,480
Pues Gnome es un escritorio, GNU/Linux, muy veterano, pero hay varias ramas. La última

25
00:02:15,480 --> 00:02:31,960
es Gnome 4 o 40 y vamos a ver qué nos trae de accesibilidad visual.

26
00:02:31,960 --> 00:02:36,320
Otra de las secciones es 2023, una odisea en el ciberespacio.

27
00:02:36,320 --> 00:02:43,080
Mire, Vey, usted está trastornado por esto. Debería tomar una píldora para la paciencia

28
00:02:43,080 --> 00:02:46,160
y una vez tranquilo pensar las cosas de nuevo.

29
00:02:46,160 --> 00:02:51,600
¿Qué está usted haciendo, David? Con nuestro David Particular, David Marzal, el Machine

30
00:02:51,600 --> 00:02:56,760
Learning y la IA abren un mundo de posibilidades para mejorar la accesibilidad en muchos aspectos

31
00:02:56,760 --> 00:03:01,560
de la vida. Con David exploraremos estos mundos artificiales. Hola, David.

32
00:03:01,560 --> 00:03:04,760
Buenas, Jorge, y resto de compañeros y compañeras.

33
00:03:04,760 --> 00:03:06,520
Preséntate brevemente para quien no te conozca.

34
00:03:06,520 --> 00:03:12,840
Pues yo soy David Marzal, de Cartagena, soy administrador de sistemas y siempre ando metido

35
00:03:12,840 --> 00:03:18,040
en mucho fregado, te trastear y tocar cosas. Entonces, esto es una buena oportunidad para

36
00:03:18,040 --> 00:03:23,640
toquetear aplicaciones y programas que sean útiles al resto de la gente en más sentidos

37
00:03:23,640 --> 00:03:29,200
de lo que es la mera utilidad práctica, sino en un sentido de accesibilidad mucho más

38
00:03:29,200 --> 00:03:31,400
amplio a lo que solemos usar.

39
00:03:31,400 --> 00:03:36,480
Básicamente vamos a hablar de proyectos molones relacionados con accesibilidad de alguna forma,

40
00:03:36,480 --> 00:03:37,480
¿verdad?

41
00:03:37,480 --> 00:03:39,880
Efectivamente. Cosas chulas que le sean útiles a la gente.

42
00:03:39,960 --> 00:03:45,320
Y cuando decimos molones, ¿son chulos y sobre todo de software libre o código abierto?

43
00:03:45,320 --> 00:03:50,320
Hombre, solo y exclusivamente. No creo que me vaya a ver viendo un lenguaje propietario

44
00:03:50,320 --> 00:03:56,120
en Windows investigando, sobre todo cuando hay tantas aplicaciones y tantos proyectos

45
00:03:56,120 --> 00:03:57,960
en software libre que se pueden usar.

46
00:03:57,960 --> 00:04:02,240
Vale, te dejo que hagas un spoiler del siguiente programa. ¿De qué vas a tocar?

47
00:04:02,240 --> 00:04:07,240
Si te siguen, lo mismo ya lo saben, porque hemos hecho algún pinito ya por ahí. Pero

48
00:04:07,400 --> 00:04:12,880
el siguiente será con, y que me perdone quien entienda realmente del término, con inteligencia

49
00:04:12,880 --> 00:04:18,320
artificial, con Whisper, sea como sea, lenguaje profundo o lenguaje de modelo. El nombre que

50
00:04:18,320 --> 00:04:25,000
tenga realmente al final es una aplicación entrenada que es capaz de traducir inclibermente

51
00:04:25,000 --> 00:04:31,280
bien de un idioma a otro, de audio a texto. Sobre todo puede servir para coger un post

52
00:04:31,280 --> 00:04:33,880
cada un vídeo y sacarle la transcripción y hacerle sustitución.

53
00:04:33,880 --> 00:04:37,680
He dicho un par de spoilers, no cuentes más, no cuentes más, que la gente ya no nos escucha

54
00:04:37,680 --> 00:04:39,280
el próximo programa.

55
00:04:39,280 --> 00:04:51,280
Córtame la mitad de lo que he dicho.

56
00:04:51,280 --> 00:04:57,760
Pues pasamos a otra sección que sería Otros mundos, otras realidades con Thais Pausada.

57
00:04:57,760 --> 00:05:02,120
Thais nos acompañará para darnos a conocer diferentes situaciones y problemas menos conocidos

58
00:05:02,120 --> 00:05:04,600
por el gran público. Hola Thais.

59
00:05:04,600 --> 00:05:05,600
Hola.

60
00:05:05,600 --> 00:05:07,160
Te presentas brevemente.

61
00:05:07,160 --> 00:05:14,000
Yo soy terapeuta ocupacional y actualmente trabajo como profesora en la Universidad de

62
00:05:14,000 --> 00:05:20,560
Coruña y formo parte de un grupo de investigación en el que tratamos de utilizar la tecnología

63
00:05:20,560 --> 00:05:26,440
para el bien común. Es decir, tratamos de buscar soluciones tecnológicas para mejorar

64
00:05:27,440 --> 00:05:34,280
la comunicación y la participación de las personas con discapacidad. En concreto, en

65
00:05:34,280 --> 00:05:41,520
la sección que voy a abordar me centraré sobre todo en cuestiones de dificultades que

66
00:05:41,520 --> 00:05:47,480
tienen en el día a día y cómo la tecnología puede ayudar a personas con discapacidad física

67
00:05:47,480 --> 00:05:49,280
y orgánica.

68
00:05:49,280 --> 00:05:51,720
Un pequeño adelanto del siguiente programa.

69
00:05:51,800 --> 00:06:01,760
Pues vamos a tener a una compañera mía como invitada, que es la coordinadora de AGAELA,

70
00:06:01,760 --> 00:06:07,360
la Asociación de Esclerosis Lateral Amiotrófica, y nos va a contar un poco cuál es la realidad

71
00:06:07,360 --> 00:06:15,360
de estas personas que tienen ELA. Es decir, que tienen una movilidad muy, muy, muy limitada,

72
00:06:15,920 --> 00:06:23,720
no tienen voz porque su aparato fonador no funciona y cómo la tecnología les puede ayudar

73
00:06:23,720 --> 00:06:28,240
en su día a día y cómo hay recursos que todavía son muy caros y que debemos trabajar

74
00:06:28,240 --> 00:06:34,440
entre todos para conseguir abaratar el precio o que sea gratuito.

75
00:06:34,440 --> 00:06:39,440
Y como veis, no disponemos de pocos colaboradores, pero también vamos a tener invitados en las

76
00:06:39,440 --> 00:06:44,440
diferentes secciones, así que no nos vamos a aburrir nada. Pues muchas gracias, Thais.

77
00:06:45,360 --> 00:07:01,160
Vamos con otra sección, como es tecnología web, con Pablo Arias. En la sección de tecnología

78
00:07:01,160 --> 00:07:07,120
web, Pablo nos hablará sobre la accesibilidad aplicada al contenido de Internet. Hola, Pablo.

79
00:07:07,120 --> 00:07:11,360
Muy buenas tardes. ¿Qué tal? Pues soy Pablo Arias, me dedico al mundo del desarrollo web

80
00:07:11,360 --> 00:07:18,280
desde hace muchos años y una de mis preocupaciones es hacer la web accesible para todo el mundo,

81
00:07:18,280 --> 00:07:22,960
por personas que lo necesitan a diario y por personas que lo vamos a necesitar con el paso

82
00:07:22,960 --> 00:07:29,000
del tiempo. Entonces necesitamos que sea accesible para todos. Efectivamente. Igual que en las

83
00:07:29,000 --> 00:07:33,480
otras secciones, ¿me puedes adelantar un poquito de qué vas a hablar en el primer programa?

84
00:07:33,480 --> 00:07:39,040
Pues vamos a hablar sobre accesibilidad web en general y por qué es importante tenerla,

85
00:07:39,040 --> 00:07:44,520
necesitan esta accesibilidad web, quienes están obligados a tener una web accesible,

86
00:07:44,520 --> 00:07:50,000
este tipo de cositas iniciales. Luego empezaremos con entrevistas y demás.

87
00:07:59,360 --> 00:08:04,880
Y en la sección diseño para todos, Jonathan Chacon nos guiará sobre diferentes temas como son la

88
00:08:04,880 --> 00:08:09,200
accesibilidad, el software libre o la integración y diseño para todos. Hola, Jonathan.

89
00:08:09,200 --> 00:08:17,920
Buenas a todos, hola. Pues en esta sección vamos a hablar de tecnología para personas y no de

90
00:08:17,920 --> 00:08:23,960
personas que tengan que pegarse con la tecnología, porque un diseño inclusivo es accesible, es

91
00:08:23,960 --> 00:08:30,440
entendible, es operable y es robusto y es para personas. Muy bien. Cuéntanos, Jonathan,

92
00:08:30,440 --> 00:08:37,720
¿cómo acabaste trabajando en tecnología? Pues yo soy bastante precoz. A los cuatro años empezó

93
00:08:39,440 --> 00:08:48,280
mi epopeya porque entró el MSX1 de un primo mío. Meses después entró un Nantran CPC y eso fue amor

94
00:08:48,280 --> 00:08:54,800
a primera vista, no sólo por las teclas de colores, sino porque tenía 64K para hacer lo que yo

95
00:08:54,800 --> 00:09:01,960
quisiese. Mi primo me enseñó unos rudimentos básicos de BASIC y ahí vi que había un gran

96
00:09:01,960 --> 00:09:07,840
mundo para hacer cosas y para aprender. Y creo que te aficionaste rápidamente a los videojuegos. De

97
00:09:07,840 --> 00:09:13,960
hecho, has diseñado varios videojuegos. Sí, es que dentro del mundo de la programación hay grandes

98
00:09:13,960 --> 00:09:21,520
totems de la programación y uno de ellos es diseñar y desarrollar un videojuego porque tienes que

99
00:09:21,520 --> 00:09:29,560
exprimir el hardware y hacer que la lógica de la aplicación sea lo más efectiva posible para que

100
00:09:29,560 --> 00:09:35,640
todo el videojuego sea fluido. Entonces, por eso me atraía de llegar a esos límites de la máquina

101
00:09:35,640 --> 00:09:41,160
e intentar romperlos. Cuéntanos Jonathan, ¿has colaborado con diferentes proyectos de software

102
00:09:41,160 --> 00:09:52,320
libres? Allá por el 2001-2002 empecé a hacer mis pinitos con aquellas distribuciones de Linux

103
00:09:52,320 --> 00:09:57,720
que había que estudiar muchísimo para poder instalarlas y que no tenían accesibilidad.

104
00:09:57,720 --> 00:10:04,360
Entonces, yo me tenía que conformar por conexiones vía telnet. Luego más adelante,

105
00:10:04,360 --> 00:10:10,520
cuando se pudieron hacer rudimentos de lectores de pantalla, para el que no lo sepa yo soy ciego,

106
00:10:10,520 --> 00:10:15,720
entonces tener una interfaz gráfica de usuario no es que no me importe, es que no lo uso. Entonces,

107
00:10:15,720 --> 00:10:21,800
pues accediendo a la terminal, haciendo esos rudimentos, luego empecé a colaborar ya cuando

108
00:10:21,800 --> 00:10:28,560
Gnome empezó a hacer cosas como Evolution, el proyecto Thunderbird, también he estado colaborando

109
00:10:28,560 --> 00:10:33,880
con la Fundación Mozilla y luego me pasé a colaborar porque descubrí curiosamente que había

110
00:10:33,880 --> 00:10:42,200
open source, no solo Linux, sino también que había en Windows, Mac, incluso en Android y en iOS,

111
00:10:42,200 --> 00:10:46,920
hay también software libre. Entonces, siempre he visto que los proyectos de software libres tienen

112
00:10:46,920 --> 00:10:51,880
muy buena voluntad, muy buenas intenciones, pero la accesibilidad se desconoce. Entonces, pues ahí

113
00:10:51,880 --> 00:10:57,160
entro yo tanto para divulgar como para arrimar el hombro para que los proyectos sean los más

114
00:10:57,160 --> 00:11:01,560
accesibles posibles. ¿Y en estos últimos años por dónde te han llevado los derroteros laborales?

115
00:11:01,920 --> 00:11:09,120
Pues ahora mismo soy accessibility lead en Kavify, no solo tengo que hacer que la aplicación de

116
00:11:09,120 --> 00:11:13,960
teléfono, tanto la de Android como la de iOS sean accesibles, sino que me enfrento a los retos

117
00:11:13,960 --> 00:11:20,840
de la accesibilidad fuera de lo digital, porque mover personas por una ciudad es un reto bastante

118
00:11:20,840 --> 00:11:29,880
interesante y apoyándonos en la tecnología, pues intentas deducir y aportar soluciones para

119
00:11:29,880 --> 00:11:34,240
que las personas mantengan su seguridad y su confortabilidad moviéndose por las

120
00:11:34,240 --> 00:11:41,080
ciudades. Aparte, sigo con mi rama de investigación tecnológica, tanto haciendo operitaje de

121
00:11:41,080 --> 00:11:47,120
dispositivos e instalaciones como investigando nuevas formas de hacer las cosas más accesibles,

122
00:11:47,120 --> 00:11:51,320
o que al menos se pueden utilizar por el mayor número posible de personas.

123
00:11:51,320 --> 00:11:55,800
Y volviendo al tema de videojuegos, ¿también has colaborado con alguna empresa para hacer

124
00:11:55,800 --> 00:12:00,600
juegos más accesibles? Sí, tanto ofreciéndoles consultorías,

125
00:12:00,600 --> 00:12:07,960
también los he desarrollado yo, y alguna conversación de te vienes con nosotros,

126
00:12:07,960 --> 00:12:14,200
pero en España se vive muy bien, entonces por ahora me sigo quedando en España y ahora mismo,

127
00:12:14,200 --> 00:12:18,760
ahora que sacas el tema de los videojuegos, hay mucho interés por los videojuegos,

128
00:12:18,760 --> 00:12:24,320
las nuevas tecnologías, eso que llaman metaverso, realidad aumentada o realidad virtual,

129
00:12:24,600 --> 00:12:31,160
y quieras o no, ahí la accesibilidad digital clásica no es suficiente, te tienes que ir

130
00:12:31,160 --> 00:12:35,160
a los límites y ahí los investigadores tecnológicos en accesibilidad somos los

131
00:12:35,160 --> 00:12:39,640
que tenemos que inventarnos las soluciones, porque lo que hay actualmente no se adapta

132
00:12:39,640 --> 00:12:44,720
por completo a estas nuevas interfaces digitales. Pues sobre accesibilidad en videojuegos también

133
00:12:44,720 --> 00:12:49,520
hablaremos en estos programas futuros. Un poquito más, Jonathan, no sé si quieres

134
00:12:49,520 --> 00:12:53,440
comentar alguna cosilla más para intentar enganchar a los futuros oyentes.

135
00:12:53,440 --> 00:13:00,240
Pues sí, hablar de que la tecnología es atractiva, la tecnología ayuda, pero nunca

136
00:13:00,240 --> 00:13:05,600
debemos olvidar que la tecnología debe estar para nosotros y nosotros no debemos tener miedo

137
00:13:05,600 --> 00:13:11,040
a la tecnología, nosotros tenemos que hacer que la tecnología sea parte de nosotros.

138
00:13:11,040 --> 00:13:16,240
Esperamos que nos acompañéis una vez al mes en esta aventura y que os gusten los contenidos

139
00:13:16,240 --> 00:13:19,440
que tenemos preparados. Hasta el próximo programa. Chao.

