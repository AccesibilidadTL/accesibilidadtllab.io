---
title: "Union Europea, Fediverso, Datos abiertos, Aplicaciones, Joomla, Talentos inclusivos"
date: 2024-11-22T07:34:11+02:00
#categories: []
author: D.Marzal
tags: [podcast, temporada1, accesibilidad, Union Europea, Fediverso, Datos abiertos, Aplicaciones, Joomla, Talentos inclusivos]
img: logo-ATL-opt.png
imgALT : ""
podcast:
  guid : 11-Accesibilidad-con-Tecnologias-libres
  #URLhost : "gnulinuxvalencia.org/audio-video/audio/"
  audio : 11-Accesibilidad-con-Tecnologias-libres
  mlength : 78379008
  iduration : "01:05:10"
  capitulos : "https://accesibilidadtl.gitlab.io/posts/atl/011/011-ATL_Chapters.json"
si :
  uri : "https://masto.es/@DavidMarzalC/113525215580825257"
transcript :
  - trurl : "https://op3.dev/e,pg=68b52970-05d8-5cb1-8bd3-d15fa3961e09/accesibilidadtl.gitlab.io/posts/atl/011/011-ATL_Subs.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=68b52970-05d8-5cb1-8bd3-d15fa3961e09/accesibilidadtl.gitlab.io/posts/atl/011/011-ATL_Subs.srt"
    trtype : application/srt
participantes :
  - person : Jorge Lama
    role : Producer
    group : Creative Direction
    img : https://accesibilidadtl.gitlab.io/images/JorgeLama.jpg
    href : https://mastodon.social/@raivenra
  - person : David Marzal
    role : Reporter
    img : "https://accesibilidadtl.gitlab.io/images/DavidMarzal.png"
    href : https://masto.es/@DavidMarzalC
  - person : Pablo Arias
    role : Reporter
    img : https://accesibilidadtl.gitlab.io/images/PabloArias-rd.png
    href : https://www.pabloarias.eu/
  - person : Thais Pousada
    role : Reporter
  - person : David Pacios
    role : Reporter
    img : "https://accesibilidadtl.gitlab.io/images/DavidPacios.jpg"
    href : https://es.linkedin.com/in/pascal-ucm
  - person : Enrique Varela
    role : Reporter
    img : "https://accesibilidadtl.gitlab.io/images/enrique-varela.jpeg"
    href : https://enriquevarela.tech/
  - person : Jonathan Chacón
    role : Reporter
    img : "https://accesibilidadtl.gitlab.io/images/JonathnChacon.png"
    href : https://tyflosaccessiblesoftware.com/

itunes:
  explicit:   # por defecto false
aliases:
  - /11
---
{{< audiohtml5 preload="none" class="text-center" >}}
___
Producido por Jorge Lama, traemos las secciones de David Marzal, Pablo Arias, Thais Pousada, Víctor, David Pacios, Enrique Varela y como productor Jorge Lama.

* 00:00 - Odisea en el ciberespacio, con [David Marzal](https://masto.es/@DavidMarzalC). Magazine de noticias.
  * Recomendaciones de [clientes para el Fediverso](https://privacy.thenexus.today/mastodon-accessibility-resources/). Semaphore, elk.zone; Mona; Pachli, Tusky; Tweescake y TWBlue.
  * [BuscaApps](https://www.buscaapps.com): Listado de aplicaciones colaborativa con filtro por categoría, sistema operativo y como de accesibles son.
  * [Web](https://accessible-eu-centre.ec.europa.eu/index_en?prefLang=es) europea de accesibilidad: Seguimiento de implenentación, guias, material, investigaciones, noticias, eventos, biblioteca digital, buenas practicas, contenido multimedia...
  * Articulo sobre las [URAs de las instituciones](https://jmdaweb.github.io/posts/ura/): Unidad Responsable de Accesibilidad.
  * Datos abiertos, [Mapcesible](https://mapcesible.fundaciontelefonica.com/home): Incorpora catorce conjuntos de datos de organismos oficiales, incluyendo del Ministerio de Agricultura y Medioambiente, ayuntamientos de diferentes ciudades (incluidos Madrid y Barcelona) y de los gobiernos autonómicos. [Enlace](https://administracionelectronica.gob.es/pae_Home/pae_Actualidad/pae_Noticias/2024/Octubre/Noticia-2024-10-29-datos-abiertos-para-navegar-por-ciudades.html)
  * Mejoras en la web y analisis automatico en cada commit [con a11y](https://gitlab.com/accesibilidadtl/accesibilidadtl.gitlab.io/-/jobs/8405018098/artifacts/browse/reports/).
* 11:22 - Accesibilidad web, con [Pablo Arias](https://www.pabloarias.eu). [Artículo en la revista de Joomla](https://magazine.joomla.org/all-issues/october-2024/keep-the-focus) de Viviana Menzel sobre focus.
* 18:32 - Otros mundos, otras realidades, con Thais Pousada. Entrevista a Irene Brage, de Talentos inclusivos.
* 36:12 - Clases con Pascal, con [David Pacios](https://es.linkedin.com/in/pascal-ucm).
* 51:39 - Érase una vez, con [Enrique Varela](https://enriquevarela.tech/).
* 59:09 - Diseño para todos, con [Jonathan Chacón](https://tyflosaccessiblesoftware.com/).
___
{{% linksubssteno %}}

{{< detalsum "Transcripción completa pinchando aquí" >}}
{{% includef "./posts/atl/011/011-ATL_Subs.txt" %}}
{{< /detalsum >}}

{{% licencia %}}
