---
title: "#01 Puesta en marcha de ATL"
date: 2023-11-30T01:01:01+01:00
#categories: []
author: D.Marzal
tags: [podcast, temporada1, accesibilidad, tecnologías libres, inteligencia artificial]
img: logo-ATL-opt.png
imgALT : ""
podcast:
  URLprefix : https://media.blubrry.com/3752802/op3.dev/e,pg=68b52970-05d8-5cb1-8bd3-d15fa3961e09/
  audio: 01-Accesibilidad-con-Tecnologias-libres
  mlength : 16310272
  iduration : "0:13:29"
  capitulos : "https://accesibilidadtl.gitlab.io/posts/atl/001/001-ATL_Chapters.json"
si :
  uri : "https://masto.es/@DavidMarzalC/114070206628547391"
transcript :
  - trurl : "https://op3.dev/e,pg=68b52970-05d8-5cb1-8bd3-d15fa3961e09/accesibilidadtl.gitlab.io/posts/atl/001/001-ATL_Subs.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=68b52970-05d8-5cb1-8bd3-d15fa3961e09/accesibilidadtl.gitlab.io/posts/atl/001/001-ATL_Subs.srt"
    trtype : text/srt
participantes :
  - person : Jorge Lama
    role : Producer
    group : Creative Direction
    img : https://accesibilidadtl.gitlab.io/images/JorgeLama.jpg
    href : https://mastodon.social/@raivenra
  - person : David Marzal
    role : Reporter
    img : "https://accesibilidadtl.gitlab.io/images/DavidMarzal.png"
    href : https://masto.es/@DavidMarzalC
  - person : Thais Pousada
    role : Reporter
    img : 
    href : 
  - person : Pablo Arias
    role : Reporter
    img : https://accesibilidadtl.gitlab.io/images/PabloArias-rd.png
    href : https://www.pabloarias.eu/
  - person : Víctor
    role : Reporter
    img : 
    href : 
  - person : Jonathan Chacón
    role : Reporter
    img : "https://accesibilidadtl.gitlab.io/images/JonathnChacon.png"
    href : https://tyflosaccessiblesoftware.com/
  - person : Kevin MacLeod
    role : Music Contributor
    group : Audio Post-Production
    img : https://accesibilidadtl.gitlab.io/images/kevinmacleod.webp
    # https://cdn.filmmusic.io/storage/user_upload/filmmusic/music/artists/kevinmacleod.jpg?width=250&height=250&aspect_ratio=1:1
    # https://e-cdn-images.dzcdn.net/images/artist/468e3c92efc9fb87f581800dc1ddd365/264x264-000000-80-0-0.jpg
    href : https://incompetech.com/music/royalty-free/index.html?isrc=USUAN2300002
itunes:
  explicit:   # por defecto false
aliases:
  - /1
  - /01
---
{{< audiohtml5 preload="none" class="text-center" >}}
___
Un podcast para hablar sobre temas de accesibilidad y tecnologías libres.

En informática, la accesibilidad incluye diferentes ayudas como pueden ser las tipografías de alto contraste o gran tamaño, magnificadores de pantalla, lectores y revisores de pantalla, programas de reconocimiento de voz, teclados adaptados y otros dispositivos apuntadores o de entrada de información.

Además, las inteligencias artificiales están empezando a ser un gran aliado para mejorar la accesibilidad en muchos aspectos. Existen podcasts y canales de vídeo que hablan de la accesibilidad centrándose en entornos Windows o de Apple porque son los más conocidos por el público generalista. Pero en este podcast queremos dar a conocer otros aspectos de la accesibilidad y su relación con otras tecnologías menos conocidas.

Tecnologías que consideramos libres y que nos parecen mejores para la sociedad, en muchos casos...
___
{{% linksubssteno %}}

{{< detalsum "Transcripción completa pinchando aquí" >}}
{{% includef "./posts/atl/001/001-ATL_Subs.txt" %}}
{{< /detalsum >}}
* Créditos de la música:
{{% m_evening %}}

{{% licencia %}}
