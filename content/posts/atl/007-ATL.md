---
title: "Diseño accesible, asistente virtual, eslibre 2024 y teclados"
date: 2024-06-25T07:07:07+02:00
#categories: []
author: D.Marzal
tags: [podcast, temporada1, accesibilidad, tecnologías libres, Inteligencia Artificial, Diseño accesible, teclados]
img: logo-ATL-opt.png
imgALT : ""
podcast:
  guid : 07-Accesibilidad-con-Tecnologias-libres
  audio: 07-Accesibilidad-con-Tecnologias-libres
  mlength : 85235712
  iduration : "01:10:53"
  capitulos : "https://accesibilidadtl.gitlab.io/posts/atl/007/007-ATL_Chapters.json"
si :
  uri : "https://masto.es/@DavidMarzalC/114070206628547391"
transcript :
  - trurl : "https://op3.dev/e,pg=68b52970-05d8-5cb1-8bd3-d15fa3961e09/accesibilidadtl.gitlab.io/posts/atl/007/007-ATL_Subs.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=68b52970-05d8-5cb1-8bd3-d15fa3961e09/accesibilidadtl.gitlab.io/posts/atl/007/007-ATL_Subs.srt"
    trtype : application/srt
participantes :
  - person : Jorge Lama
    role : Producer
    group : Creative Direction
    img : https://accesibilidadtl.gitlab.io/images/JorgeLama.jpg
    href : https://mastodon.social/@raivenra
  - person : Eva Marco
    role : Reporter
    href : https://fosstodon.org/@eva_m
  - person : David Marzal
    role : Reporter
    img : "https://accesibilidadtl.gitlab.io/images/DavidMarzal.png"
    href : https://masto.es/@DavidMarzalC
  - person : Lorenzo Carbonell
    role : Guest
    href : https://atareao.es/
    img: https://atareao.es/wp-content/uploads/2021/08/lorenzo-carbonell-800x800-1.webp
  - person : Pablo Arias
    role : Reporter
    img : https://accesibilidadtl.gitlab.io/images/PabloArias-rd.png
    href : https://www.pabloarias.eu/
  - person : Thais Pousada
    role : Reporter
  - person : Jonathan Chacón
    role : Reporter
    img : "https://accesibilidadtl.gitlab.io/images/JonathnChacon.png"
    href : https://tyflosaccessiblesoftware.com/
itunes:
  explicit:   # por defecto false
aliases:
  - /7
  - /07
---
{{< audiohtml5 preload="none" class="text-center" >}}
___
Programa 07:

* Rompiendo barreras, con Eva Marco. En la que nos habla sobre el "Diseño accesible". Explica por qué es importante tenerlo en cuenta al inicio de tu proyecto y se ven [ejemplos](https://www.w3.org/WAI/ARIA/apg/patterns/) para ir aplicando en tu proyecto.
* 00:13:19 Odisea en el ciberespacio, con [David Marzal](https://masto.es/@DavidMarzalC). Entrevista a Lorenzo Carbonell (Atareao) en la que hablamos de las posibilidades del FLOSS y las automatización con los nuevos LLM / "IAs".
  * Como su último proyecto de asistente virtual para GNU/Linux en [Atareao 601](https://atareao.es/podcast/inteligencia-artificial-en-local-con-docker/).
* 00:36:27 Accesibilidad web, con [Pablo Arias](https://www.pabloarias.eu). Nos habla sobre la navegación con el teclado por las páginas web. 
* 00:46:53 Otros mundos, otras realidades, con Thais Pousada. Comenta su paso por la [esLibre 2024](https://eslib.re/2024/).
* 01:02:19 Diseño para todos, con [Jonathan Chacón](https://tyflosaccessiblesoftware.com/). Nos habla de teclados opensource.
___
{{% linksubssteno %}}

{{< detalsum "Transcripción completa pinchando aquí" >}}
{{% includef "./posts/atl/007/007-ATL_Subs.txt" %}}
{{< /detalsum >}}
* Créditos de la música:
{{% m_evening %}}

{{% licencia %}}
