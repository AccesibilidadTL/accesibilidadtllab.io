---
title: Contacto
tags: [contacto]
aliases:
    - /contacto
    - /contact
---
Buenas, tenemos el placer de presentaros este podcast que pretende dar voz a temas de accesibilidad pero desde la perspectiva de la cultura y el software libre.

Os dejamos algunos enlaces de interes aquí centralizados.

* ¿Quienes somos?
    * En el [episodio 001](https://accesibilidadtl.gitlab.io/posts/atl/001-presentacion/) lo comentamos.

* Feeds RSS: https://accesibilidadtl.gitlab.io/feed

* Correo:
    * AccesibilidadTL@protonMail.com

* RRSS/Medios:
    * David Marzal en el Fediverso : [@DavidMarzalC@masto.es](https://masto.es/@DavidMarzalC)
        * Notificaciones de episodios en el Fediverso : @6706483@ap.podcastindex.org

* Puedes ver en [Podnews](https://podnews.net/podcast/iaxjt) enlaces a todas las plataformas disponibles. Nosotros recomendamos:
    * [PodFriend](https://web.podfriend.com/podcast/1719253810) - Es Software Libre y soporta cápitulos y transcripción, Creditos y Recomendaciones.
    * [Podverse.fm](https://podverse.fm/podcast/mlKzwwOXoG) - Es Software Libre y soporta cápitulos y transcripción.
* Y en [Episodes.fm](https://episodes.fm/1719253810) puedes acceder a tu aplicación de Podcast favorita, aunque nosotros recomendamos [AntennaPod](https://f-droid.org/id/packages/de.danoeh.antennapod/), que es FLOSS y soporta cápitulos y transcripción.

* Código fuente del proyecto:
    * [Gitlab](https://gitlab.com/kdeexpress/accesibilidadtl.gitlab.io)