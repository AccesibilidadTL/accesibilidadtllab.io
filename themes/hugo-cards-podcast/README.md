# Hugo Podcast cards

A Hugo theme, ported from [hugo-cards](https://github.com/bul-ikana/hugo-cards)

![Screenshot](https://github.com/bul-ikana/hugo-cards/blob/master/images/screenshot.png)

## Features

Responsive theme built on bootstrap. Great for showcasing content with featured images.

Easily add disqus, google analytics, social links and a facebook like box.

## Customization

### Images
To change the featured image of your post, you must provide a `img` param in your post front-matter. This theme now supports [page resources](https://gohugo.io/content-management/page-resources/) so your featured image should be in the same directory than your `index.md` post file.

If you don't include one, a default image will be used. You can also customize this default image by changing the `defaultImage` param in your theme config. Be aware that this default image should be located on your `static/images` directory

### CSS
There is a `custom.css` file included where you can write and override any style you need. If you need additional files, you can add their relative links to the `custom_css` param in the theme config.


## Configuration

Please see the sample [`hugo.toml`](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/hugo.toml?ref_type=heads).

### Social links

#### facebook
A link to your facebook profile or page. This will show in the footer.

#### quora
A link to your quora profile. This will show in the footer.

#### twitter
A link to your twitter profile. This will show in the footer.

#### github
A link to your github profile. This will show in the footer.

#### email
Your email address. This will show in the footer and will opes as a `mailto:`.

### Tracking and comments

#### analytics
The Google Analytics tracking ID

#### disqus
The disqus_thread ID
